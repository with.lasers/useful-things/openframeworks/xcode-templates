# Xcode Templates for OpenFrameworks

Templates to make life a little easier using OpenFrameworks in Xcode

## Installation

If it doesn't exist yet, you will need to create the directory `~/Library/Developer/Xcode/Templates` which from the terminal you can do with the following command in Terminal

```
mkdir ~/Library/Developer/Xcode/Templates
```

Then either download this repo, expand the .zip file, and copy the whole folder as `OpenFrameworks` into `~/Library/Developer/Xcode/Templates`,

or

Clone this repo into the folder by using the following

```
cd ~/Library/Developer/Xcode/Templates
git clone https://gitlab.com/with.lasers/useful-things/openframeworks/xcode-templates.git
```

If you clone the repo, you can in future update the templates with any changes made in here using

```
git pull
```

## Templates

Current templates are

* Header only file - by default comes with `#pragma once` directive, and `#includes ofMain.h` lines
* cpp file with optional header, and uses `#pragma once` and has the `ofMain.h` include 

You should find the Template in it's own section titled OpenFrameworks when you create a new file (if you didn't rename the folder on download, it will probabaly be OpenFrameworks-master)

![Xcode  add file dialog](docs/newFileDialog.png)

## Want to tweak your template

Have a look at this on [post][1] on Thoughtbot (which I used to make this), so if you want to add any standard headers, or such like you can - there are a number of variables which are replaced that have the form `___VARIABLE_NAME___` like `___FULLUSERNAME__` and `___FILEBASENAME___` which will put your user name, and the name chosed in the file dialog into the template. 

Also, you'll see there are folders for Default and WithHeader - these reflect if you tick the `Also create header file` option in the create dialog. With Default being used if this is not selcted, and WithHeader used if it is selected.

![Xcode create header file option](docs/createHeaderFile.png)

## Future Templates

I created this, as I was bored of always having to change the include line when I create a new file (small but tedious things!). At present this seems to be the only one I need, however I may well create some for…

* Creating an FBOSource when using [ofxPiMapper][2]
* Some of my standard helper classes I'm slowly creating and reusing
* Template for GUI creation with [ofxDatGui][3]

If you create your own, feel free to submit a pull request and I'll add them to the repo


## Known Issues

* Icons need updating for latest XCode (13)
* Icon for Header is the c++ file icon



[1]:https://thoughtbot.com/blog/creating-custom-xcode-templates
[2]:https://ofxpimapper.com
[3]:https://github.com/braitsch/ofxDatGui


